﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using DelaunayVoronoi;

//using SixLabors.ImageSharp;
//using SixLabors.ImageSharp.Processing;
//using SixLabors.ImageSharp.Drawing.Processing;

namespace filler
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Random rand = new Random();
        Bitmap sourceImage;    
        Bitmap reduced;
        Bitmap filled;

        Bitmap src;
        Bitmap trg;
        private void pictureBox1_Click(object sender, EventArgs e) { }


        private delegate void SetTarget();

        private void Load_Click(object sender, EventArgs e)
        {
            if (!locked)
            {
                sourceImage = (Bitmap)Image.FromFile(LoadName.Text);
                pb.Image = sourceImage;
            }
        }
        private void Reduce_Click(object sender, EventArgs e)
        {
            if (!locked)
            {
                const int precision = 10000;
                reduced = new Bitmap(sourceImage.Width, sourceImage.Height);
                Graphics reduction = Graphics.FromImage(reduced);
                int prob = (int)(precision * Convert.ToDouble((float)ReductionEmount.Value / (float)ReductionEmount.Maximum));          //add checking
                int pixels = reduced.Width * reduced.Height;
                for (int i = 0; i < pixels; i++)
                {
                    if (rand.Next(0, precision) > prob)
                    {
                        reduced.SetPixel(i % reduced.Width, i / reduced.Width, sourceImage.GetPixel(i % reduced.Width, i / reduced.Width));

                    }
                    else
                        reduced.SetPixel(i % reduced.Width, i / reduced.Width, Color.FromArgb(0, 0, 0, 0));

                }
                pb.Image = reduced;
            }
        }
        private void Fill_Click(object sender, EventArgs e)
        {
            FillKerneled(false, existingOverwriteDisabled.Checked, ReuseLastIteration.Checked);
        }
        private void FillDoublePass_Click(object sender, EventArgs e)
        {
            FillKerneled(true, existingOverwriteDisabled.Checked, ReuseLastIteration.Checked);
        }
        List<float> kernel;
        private void UpdateKernel(float expectedPasses = 1.0f)
        {
            List<float> kern = new List<float>();
            int kernelSize = Convert.ToInt32(KernelSize.Text);
            for (int i = 0; i < kernelSize * 2 + 1; i++)
            {
                kern.Add(0);
            }
            BuildKernel(kern, expectedPasses);
            kernel = kern;
        }
        private void BuildKernel(List<float> kernel, float expectedPasses = 1.0f)         //must have odd count
        {
            int steps = (kernel.Count + 1) / 2;
            float falloff = 1.0f / steps;
            for (int i = 0; i < steps; i++)
            {
                float strength = (i + 1) * falloff;
                kernel[i] = strength;
                kernel[kernel.Count - i - 1] = strength;
            }
            float sum = 0.0f;
            for (int i = 0; i < kernel.Count; i++)
                sum += kernel[i];
            for (int i = 0; i < kernel.Count; i++)
            {
                kernel[i] /= (sum * expectedPasses);           //2 because 2 passes
            }

        }
        public bool tpi;//targetPreviousIteration
        public bool locked = false;

        private void FillKerneled(bool doublePass, bool onlyUnfilled, bool targetPreviousIteration, bool threadAwait = false)
        {
            if (!locked|| threadAwait)
            {
                locked = true;
                tpi = targetPreviousIteration;
                LoopPixels pixelLooper;
                FillPixel pixelFiller;

                Thread t;

                if (doublePass)
                {
                    UpdateKernel(2);
                    pixelFiller = FillDoublePass;
                }
                else
                {
                    UpdateKernel(1);
                    pixelFiller = FillSinglePass;
                }

                if (onlyUnfilled)
                {
                    pixelLooper = LoopUnfilled;
                    t = new Thread(LoopUnfilledThread);
                }
                else
                {
                    pixelLooper = LoopAll;
                    t = new Thread(LoopAllThread);
                }

                if (targetPreviousIteration)
                {
                    src = filled;
                    trg = new Bitmap(sourceImage.Width, sourceImage.Height);
                }
                else
                {
                    filled = new Bitmap(sourceImage.Width, sourceImage.Height);
                    trg = filled;
                    src = reduced;
                }
                int pix = filled.Width * filled.Height;
                int kernelSize = (kernel.Count - 1) / 2;


                LoopPack lp = new LoopPack { pixels = pix, kernelSize = kernelSize, pixelFiller = pixelFiller };
                t.Start(lp);
                if (threadAwait)
                    t.Join();
            }
            //pixelLooper(pixels, kernelSize, pixelFiller);
        }

        public class LoopPack
        {
            public int pixels { get; set; }
            public int kernelSize { get; set; }
            public FillPixel pixelFiller { get; set; }
        }
        delegate void LoopPixels(int pixels, int kernelSize, FillPixel pixelFiller);
        delegate void LoopPixelsThread(Object obj);

        private void LoopAll(int pixels, int kernelSize, FillPixel pixelFiller)
        {
            for (int i = 0; i < pixels; i++)
            {
                pixelFiller(i, kernelSize);
            }
        }
        private void LoopAllThread(Object obj)
        {
            LoopPack lp = (LoopPack)obj;
            prBr.Maximum = lp.pixels;
            prBr.Value = 0;
            for (int i = 0; i < lp.pixels; i++)
            {
                if(i%300==0)
                    prBr.Value = i;
                lp.pixelFiller(i, lp.kernelSize);
            }
            if (tpi)
            {
                filled = trg;
            }
            pb.Image = filled;
            locked = false;
                
        }
        private void LoopUnfilled(int pixels, int kernelSize, FillPixel pixelFiller)
        {
            for (int i = 0; i < pixels; i++)
            {
                if (src.GetPixel(i % trg.Width, i / trg.Width).A == 0)
                {
                    pixelFiller(i, kernelSize);
                }
                else
                {
                    SetPixSource(i);
                }
            }
        }
        private void LoopUnfilledThread(Object obj)
        {
            LoopPack lp = (LoopPack)obj;
            prBr.Maximum = lp.pixels;
            prBr.Value = 0;
            for (int i = 0; i < lp.pixels; i++)
            {
                if (src.GetPixel(i % trg.Width, i / trg.Width).A == 0)
                {
                    lp.pixelFiller(i, lp.kernelSize);
                    if (i % 300 == 0)
                        prBr.Value = i;
                }
                else
                {
                    if (i % 300 == 0)
                        prBr.Value = i;
                    SetPixSource(i);
                }
            }
            if (tpi)
            {
                filled = trg;
            }
            pb.Image = filled;
            locked = false;
        }

        private class PixelData
        {
            public float a = 0;
            public float r = 0;
            public float g = 0;
            public float b = 0;
            public float metPixels = 0.0f;
        }

        public delegate void FillPixel(int i, int kernelSize);
        private void FillDoublePass(int i, int kernelSize)
        {
            AccumulatePass(i, i / trg.Width, trg.Height, kernelSize, true);
            AccumulatePass(i, i % trg.Width, trg.Width, kernelSize, false);
        }

        private void FillSinglePass(int i, int kernelSize)
        {
            AccumulateAll(i, kernelSize);
        }
        void AccumulatePass(int i, int curPos, int limPos, int kernelSize, bool vertical)
        {
            GetSourceColor gsc;
            if (vertical)
                gsc = GetVerticalPass;
            else
                gsc = GetHorizontalPass;
            PixelData pd = new PixelData();

            for (int j = -kernelSize; j < kernelSize + 1; j++)
            {
                var endPos = j + curPos;
                if (endPos >= 0 && endPos < limPos)
                {
                    AddColor(gsc(i, endPos), j, pd, kernelSize);
                }
            }
            ApplyDoublePass(i, pd, vertical);
        }
        void AccumulateAll(int i, int kernelSize)
        {
            int curPosJ = i / trg.Width;
            int limPosJ = trg.Height;
            
            int curPosK = i % trg.Width;
            int limPosK = trg.Width;

            PixelData pd = new PixelData();

            for (int j = -kernelSize; j < kernelSize + 1; j++)
            {
                var endPosJ = j + curPosJ;
                if (endPosJ >= 0 && endPosJ < limPosJ)
                {
                    for (int k = -kernelSize; k < kernelSize + 1; k++)
                    {
                        var endPosK = k + curPosK;
                        if (endPosK >= 0 && endPosK < limPosK)
                        {
                            AddColor(src.GetPixel(endPosK, endPosJ), j, pd, kernelSize);
                        }
                    }
                }
            }
            ApplyAccumulated(i, pd);
        }

        delegate Color GetSourceColor(int i, int endPos);
        Color GetVerticalPass(int i, int endPos)
        {
            return src.GetPixel(i % trg.Width, endPos); ;
        }
        Color GetHorizontalPass(int i, int endPos)
        {
            return src.GetPixel(endPos, i / trg.Width); 
        }
        private void ApplyDoublePass(int i, PixelData pd, bool vertical)
        {
            if (pd.metPixels != 0.0f)
            {
                pd.metPixels *= 2;     //normalize
                BakePD(pd);
                if (vertical)
                    SetPix(pd, i);
                else
                    SetPixBlend(pd, i);
            }
        }
        private void ApplyAccumulated(int i, PixelData pd)
        {
            if (pd.metPixels != 0.0f)
            {
                BakePD(pd);
                SetPix(pd, i);
            }
        }
        private void BakePD(PixelData pd)
        {
            pd.a /= pd.metPixels;
            pd.r /= pd.metPixels;
            pd.g /= pd.metPixels;
            pd.b /= pd.metPixels;
        }   
        private void SetPix(PixelData pd, int i)
        {
            trg.SetPixel(i % trg.Width, i / trg.Width, Color.FromArgb((int)pd.a, (int)pd.r, (int)pd.g, (int)pd.b));
        }
        private void SetPixBlend(PixelData pd, int i)
        {
            var prev = trg.GetPixel(i % src.Width, i / src.Width);
            trg.SetPixel(i % trg.Width, i / trg.Width,             //blending
                            Color.FromArgb((int)pd.a + prev.A, (int)pd.r + prev.R, (int)pd.g + prev.G, (int)pd.b + prev.B));
        }
        private void SetPixSource(int i)
        {
            trg.SetPixel(i % trg.Width, i / trg.Width, src.GetPixel(i % trg.Width, i / trg.Width));
        }
        private void AddColor(Color col,int step, PixelData pd, int kernelSize)
        {
            if (col.A != 0)
            {
                pd.a += col.A * kernel[step + kernelSize];
                pd.r += col.R * kernel[step + kernelSize];
                pd.g += col.G * kernel[step + kernelSize];
                pd.b += col.B * kernel[step + kernelSize];
                pd.metPixels += kernel[step + kernelSize];                //becomes 1 if the kernel was full
            }
        }
        private List<DelaunayVoronoi.Point> ExtractPoints(Bitmap bmp)
        {
            var pts = new List<DelaunayVoronoi.Point>();
            for (int i = 0; i < bmp.Width * bmp.Height; i++)
                if(i / bmp.Height<bmp.Height)
                    if (bmp.GetPixel(i % bmp.Width, i / bmp.Height).A != 0) pts.Add(new DelaunayVoronoi.Point(i % bmp.Width, i / bmp.Height));
            return pts;
        }

        private void BuildDel(Object obj)
        {
            var pts = (List<DelaunayVoronoi.Point>)obj;
            delaunay.prBr = prBr;
            DrawTriangles(delaunay.BowyerWatson(pts, src.Width, src.Height));
            CleanAfterDrawing();
            locked = false;
        }
        private DelaunayTriangulator delaunay = new DelaunayTriangulator();
        private void button1_Click(object sender, EventArgs e)
        {
            if (!locked)
            {
                locked = true;
                if (ReuseLastIteration.Checked)
                {
                    src = filled;
                    trg = new Bitmap(sourceImage.Width, sourceImage.Height);
                }
                else
                {
                    filled = new Bitmap(sourceImage.Width, sourceImage.Height);
                    trg = filled;
                    src = reduced;
                }
                var pts = ExtractPoints(src);
                Thread t = new Thread(BuildDel);
                t.Start(pts);                
            }
        }
        private void DrawTriangles(IEnumerable<Triangle> del)
        {
        Graphics gfx = Graphics.FromImage(trg);
        foreach (var t in del)
                {

                    var triang = t.Vertices;

        PointF p0 = new PointF((float)triang[0].X, (float)triang[0].Y);
        PointF p1 = new PointF((float)triang[1].X, (float)triang[1].Y);
        PointF p2 = new PointF((float)triang[2].X, (float)triang[2].Y);


        var col0 = src.GetPixel((int)p0.X, (int)p0.Y);
        var col1 = src.GetPixel((int)p1.X, (int)p1.Y);
        var col2 = src.GetPixel((int)p2.X, (int)p2.Y);

        int meanA = (col0.A + col1.A + col2.A) / 3;
        int meanR = (col0.R + col1.R + col2.R) / 3;
        int meanG = (col0.G + col1.G + col2.G) / 3;
        int meanB = (col0.B + col1.B + col2.B) / 3;

        Color[] colors = { col0, col1, col2 };

        p0.X += 0.95f;
                    p1.X += 0.95f;
                    p2.X += 0.95f;
                    p0.Y += 0.35f;
                    p1.Y += 0.35f;
                    p2.Y += 0.35f;

                    PointF[] trig =
                    {
                    p0, p1, p2
                };

        System.Drawing.Drawing2D.PathGradientBrush gradientBrush = new System.Drawing.Drawing2D.PathGradientBrush(trig);
        gradientBrush.CenterColor = Color.FromArgb(meanA, meanR, meanG, meanB);
                    gradientBrush.SurroundColors = colors;

                    gfx.FillPolygon(gradientBrush, trig);


                }
    filled = trg;
                pb.Image = trg;}
        private void CleanAfterDrawing()
        {
            var prev = KernelSize.Text;

            try
            {
                KernelSize.Text = "1";
            }
            catch (System.Exception)
            { }
            FillKerneled(true, true, true,true);
            KernelSize.Text = prev;
        }
    }
}