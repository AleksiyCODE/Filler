﻿
using System.Drawing;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;

using System.ComponentModel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using DelaunayVoronoi;


namespace DelaunayVoronoi
{

    public class Triangle
    {
        public Point[] Vertices { get; } = new Point[3];
        public Point Circumcenter { get; private set; }
        public double RadiusSquared;

        public Triangle(Point point1, Point point2, Point point3)
        {
            Vertices[0] = point1;
            Vertices[1] = point3;
            Vertices[2] = point2;
            UpdateCircumcircle();
        }

        private void UpdateCircumcircle()
        {
            // https://codefound.wordpress.com/2013/02/21/how-to-compute-a-circumcircle/#more-58
            var p0 = Vertices[0];
            var p1 = Vertices[1];
            var p2 = Vertices[2];
            var dA = p0.X * p0.X + p0.Y * p0.Y;
            var dB = p1.X * p1.X + p1.Y * p1.Y;
            var dC = p2.X * p2.X + p2.Y * p2.Y;

            var aux1 = (dA * (p2.Y - p1.Y) + dB * (p0.Y - p2.Y) + dC * (p1.Y - p0.Y));
            var aux2 = -(dA * (p2.X - p1.X) + dB * (p0.X - p2.X) + dC * (p1.X - p0.X));
            var div = (2 * (p0.X * (p2.Y - p1.Y) + p1.X * (p0.Y - p2.Y) + p2.X * (p1.Y - p0.Y)));

            if (div == 0)
            {
                throw new DivideByZeroException();
            }

            var center = new Point(aux1 / div, aux2 / div);
            Circumcenter = center;
            RadiusSquared = (center.X - p0.X) * (center.X - p0.X) + (center.Y - p0.Y) * (center.Y - p0.Y);
        }

        public bool SharesEdgeWith(Triangle triangle)
        {
            var sharedVertices = Vertices.Where(o => triangle.Vertices.Contains(o)).Count();
            return sharedVertices == 2;
        }

        public bool IsPointInsideCircumcircle(Point point)
        {
            var d_squared = (point.X - Circumcenter.X) * (point.X - Circumcenter.X) +
                (point.Y - Circumcenter.Y) * (point.Y - Circumcenter.Y);
            return d_squared < RadiusSquared;
        }
    }
    public class Point
    {
        public double X { get; }
        public double Y { get; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
    public class Edge
    {
        public Point Point1 { get; }
        public Point Point2 { get; }

        public Edge(Point point1, Point point2)
        {
            Point1 = point1;
            Point2 = point2;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != GetType()) return false;
            var edge = obj as Edge;

            var samePoints = Point1 == edge.Point1 && Point2 == edge.Point2;
            var samePointsReversed = Point1 == edge.Point2 && Point2 == edge.Point1;
            return samePoints || samePointsReversed;
        }

        public override int GetHashCode()
        {
            int hCode = (int)Point1.X ^ (int)Point1.Y ^ (int)Point2.X ^ (int)Point2.Y;
            return hCode.GetHashCode();
        }
    }

    public class DelaunayTriangulator
    {
        public ProgressBar prBr;
        private double MaxX { get; set; }
        private double MaxY { get; set; }
        private IEnumerable<Triangle> border;
        

        public IEnumerable<Point> GeneratePoints(int amount, double maxX, double maxY)
        {
            MaxX = maxX;
            MaxY = maxY;

            var point0 = new Point(0, 0);
            var point1 = new Point(0, MaxY);
            var point2 = new Point(MaxX, MaxY);
            var point3 = new Point(MaxX, 0);
            var points = new List<Point>() { point0, point1, point2, point3 };
            var tri1 = new Triangle(point0, point1, point2);
            var tri2 = new Triangle(point0, point2, point3);
            border = new List<Triangle>() { tri1, tri2 };

            var random = new Random();
            for (int i = 0; i < amount - 4; i++)
            {
                var pointX = random.NextDouble() * MaxX;
                var pointY = random.NextDouble() * MaxY;
                points.Add(new Point(pointX, pointY));
            }
            return points;
        }

        //WIKI
//The Bowyer–Watson algorithm is an incremental algorithm.
//It works by adding points, one at a time, to a valid Delaunay triangulation of a subset of the desired points.
//After every insertion, any triangles whose circumcircles contain the new point are deleted, leaving a star-shaped polygonal hole which is then re-triangulated using the new point.
//By using the connectivity of the triangulation to efficiently locate triangles to remove, the algorithm can take O(N log N) operations to triangulate N points, 
//although special degenerate cases exist where this goes up to O(N2).           
            public IEnumerable<Triangle> BowyerWatson(IEnumerable<Point> points, int width, int height)
        {
            var point0 = new Point(-1, -1);
            var point1 = new Point(-1, height);
            var point2 = new Point(width, height);
            var point3 = new Point(width, -1);
            var tri1 = new Triangle(point0, point1, point2);
            var tri2 = new Triangle(point0, point2, point3);
            border = new List<Triangle>() { tri1, tri2};               //encloses all image

            var triangulation = new HashSet<Triangle>(border);              //set because represents mathematical collention

         
            int i = 0;
            try
            {
                prBr.Maximum = points.Count();
            }
            catch (System.Exception)
            { }
            foreach (var point in points)
            {
                i++;
                if (i % 100 == 0)
                    try
                    {
                        prBr.Value = i;
                    }
                    catch (System.Exception)
                    { }

                var badTriangles = FindBadTriangles(point, triangulation);          //find bad
                var polygon = FindHoleBoundaries(badTriangles);                 //find bad border

                triangulation.RemoveWhere(o => badTriangles.Contains(o));       //remove bad

                foreach (var edge in polygon)       
                {
                    if (edge.Point1.X == edge.Point2.X && edge.Point1.X == point.X ||
                        edge.Point1.Y == edge.Point2.Y && edge.Point1.Y == point.Y)             //avoid virojdennie triangles
                        continue;             
                    triangulation.Add(new Triangle(point, edge.Point1, edge.Point2));
                }

            }
            List<Triangle> forRemoval = new List<Triangle>(); ;
            foreach (var t in triangulation)
            {
                if (t.Vertices[0].X == -1 ||
                    t.Vertices[1].X == -1 ||
                    t.Vertices[2].X == -1 ||
                    t.Vertices[0].Y == -1 ||
                    t.Vertices[1].Y == -1 ||
                    t.Vertices[2].Y == -1 ||
                    t.Vertices[0].X == width ||
                    t.Vertices[1].X == width ||
                    t.Vertices[2].X == width ||
                    t.Vertices[0].Y == height ||
                    t.Vertices[1].Y == height ||
                    t.Vertices[2].Y == height)
                    forRemoval.Add(t);
            }
            foreach (var t in forRemoval)
                triangulation.Remove(t);

            try
            {
                prBr.Value = points.Count();
            }
            catch (System.Exception)
            { }
            return triangulation;
        }

        private List<Edge> FindHoleBoundaries(ISet<Triangle> badTriangles)
        {
            var edges = new List<Edge>();
            foreach (var triangle in badTriangles)
            {
                edges.Add(new Edge(triangle.Vertices[0], triangle.Vertices[1]));
                edges.Add(new Edge(triangle.Vertices[1], triangle.Vertices[2]));
                edges.Add(new Edge(triangle.Vertices[2], triangle.Vertices[0]));
            }
            var boundaryEdges = edges.GroupBy(o => o).Where(o => o.Count() == 1).Select(o => o.First());
            return boundaryEdges.ToList();
        }


        private ISet<Triangle> FindBadTriangles(Point point, HashSet<Triangle> triangles)
        {
            var badTriangles = triangles.Where(o => o.IsPointInsideCircumcircle(point));
            return new HashSet<Triangle>(badTriangles);
        }
    }
}