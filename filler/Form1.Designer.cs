﻿
namespace filler
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load = new System.Windows.Forms.Button();
            this.Reduce = new System.Windows.Forms.Button();
            this.FillFull = new System.Windows.Forms.Button();
            this.LoadName = new System.Windows.Forms.TextBox();
            this.pb = new System.Windows.Forms.PictureBox();
            this.ReductionEmount = new System.Windows.Forms.TrackBar();
            this.FillDoubleCheap = new System.Windows.Forms.Button();
            this.ReuseLastIteration = new System.Windows.Forms.CheckBox();
            this.KernelSize = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.existingOverwriteDisabled = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.prBr = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReductionEmount)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Load
            // 
            this.Load.Location = new System.Drawing.Point(1259, 13);
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(75, 23);
            this.Load.TabIndex = 1;
            this.Load.Text = "Load";
            this.Load.UseVisualStyleBackColor = true;
            this.Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // Reduce
            // 
            this.Reduce.Location = new System.Drawing.Point(1259, 48);
            this.Reduce.Name = "Reduce";
            this.Reduce.Size = new System.Drawing.Size(75, 23);
            this.Reduce.TabIndex = 2;
            this.Reduce.Text = "Reduce";
            this.Reduce.UseVisualStyleBackColor = true;
            this.Reduce.Click += new System.EventHandler(this.Reduce_Click);
            // 
            // FillFull
            // 
            this.FillFull.Location = new System.Drawing.Point(1259, 145);
            this.FillFull.Name = "FillFull";
            this.FillFull.Size = new System.Drawing.Size(75, 23);
            this.FillFull.TabIndex = 3;
            this.FillFull.Text = "Fill";
            this.FillFull.UseVisualStyleBackColor = true;
            this.FillFull.Click += new System.EventHandler(this.Fill_Click);
            // 
            // LoadName
            // 
            this.LoadName.Location = new System.Drawing.Point(1000, 12);
            this.LoadName.Name = "LoadName";
            this.LoadName.Size = new System.Drawing.Size(253, 23);
            this.LoadName.TabIndex = 7;
            this.LoadName.Text = "CatSmall.png";
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(12, 22);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(861, 731);
            this.pb.TabIndex = 10;
            this.pb.TabStop = false;
            // 
            // ReductionEmount
            // 
            this.ReductionEmount.Location = new System.Drawing.Point(1000, 48);
            this.ReductionEmount.Maximum = 100;
            this.ReductionEmount.Name = "ReductionEmount";
            this.ReductionEmount.Size = new System.Drawing.Size(253, 45);
            this.ReductionEmount.TabIndex = 11;
            this.ReductionEmount.TickStyle = System.Windows.Forms.TickStyle.None;
            this.ReductionEmount.Value = 98;
            // 
            // FillDoubleCheap
            // 
            this.FillDoubleCheap.Location = new System.Drawing.Point(1259, 174);
            this.FillDoubleCheap.Name = "FillDoubleCheap";
            this.FillDoubleCheap.Size = new System.Drawing.Size(75, 23);
            this.FillDoubleCheap.TabIndex = 12;
            this.FillDoubleCheap.Text = "Fill cheap";
            this.FillDoubleCheap.UseVisualStyleBackColor = true;
            this.FillDoubleCheap.Click += new System.EventHandler(this.FillDoublePass_Click);
            // 
            // ReuseLastIteration
            // 
            this.ReuseLastIteration.AutoSize = true;
            this.ReuseLastIteration.Location = new System.Drawing.Point(6, 79);
            this.ReuseLastIteration.Name = "ReuseLastIteration";
            this.ReuseLastIteration.Size = new System.Drawing.Size(165, 19);
            this.ReuseLastIteration.TabIndex = 14;
            this.ReuseLastIteration.Text = "Use last iteration as source";
            this.ReuseLastIteration.UseVisualStyleBackColor = true;
            // 
            // KernelSize
            // 
            this.KernelSize.Location = new System.Drawing.Point(1073, 146);
            this.KernelSize.Name = "KernelSize";
            this.KernelSize.Size = new System.Drawing.Size(23, 23);
            this.KernelSize.TabIndex = 15;
            this.KernelSize.Text = "4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1102, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Kernel size";
            // 
            // existingOverwriteDisabled
            // 
            this.existingOverwriteDisabled.AutoSize = true;
            this.existingOverwriteDisabled.Location = new System.Drawing.Point(1073, 174);
            this.existingOverwriteDisabled.Name = "existingOverwriteDisabled";
            this.existingOverwriteDisabled.Size = new System.Drawing.Size(184, 19);
            this.existingOverwriteDisabled.TabIndex = 17;
            this.existingOverwriteDisabled.Text = "Don\'t overwrite existing pixels";
            this.existingOverwriteDisabled.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(380, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Delaunay ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(1053, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 100);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.ReuseLastIteration);
            this.groupBox2.Location = new System.Drawing.Point(879, 99);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(478, 167);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            // 
            // prBr
            // 
            this.prBr.Location = new System.Drawing.Point(879, 272);
            this.prBr.Name = "prBr";
            this.prBr.Size = new System.Drawing.Size(478, 23);
            this.prBr.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 808);
            this.Controls.Add(this.prBr);
            this.Controls.Add(this.existingOverwriteDisabled);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.KernelSize);
            this.Controls.Add(this.FillDoubleCheap);
            this.Controls.Add(this.ReductionEmount);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.LoadName);
            this.Controls.Add(this.FillFull);
            this.Controls.Add(this.Reduce);
            this.Controls.Add(this.Load);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReductionEmount)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Load;
        private System.Windows.Forms.Button Reduce;
        private System.Windows.Forms.Button FillFull;
        private System.Windows.Forms.TextBox LoadName;
        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.TrackBar ReductionEmount;
        private System.Windows.Forms.Button FillDoubleCheap;
        private System.Windows.Forms.CheckBox ReuseLastIteration;
        private System.Windows.Forms.TextBox KernelSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox existingOverwriteDisabled;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ProgressBar prBr;
    }
}

